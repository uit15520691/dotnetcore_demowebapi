﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApi.Contexts;
using WebApi.Controllers;
using WebApi.Models;
using WebApi.Service;

namespace WebApiTests.Tests
{
    [TestClass]
    public class LoginControllerTest
    {

        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public async System.Threading.Tasks.Task TestLogin_SuperAdmin_SuccessAsync()
        {
            // arrange
            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup(mCf => mCf["Jwt:SecretKey"]).Returns("nndangquangnndangquangnndangquangnndangquang");

            DbContextOptions<ApplicationContext> mockOption = new DbContextOptions<ApplicationContext>();
            var mockContext = new Mock<ApplicationContext>(mockOption);

            var mockTeacherService = new Mock<TeacherService>(mockContext.Object);
            LoginController controler = new LoginController(mockConfig.Object, mockTeacherService.Object);
            var loginInfo = new Teacher
            {
                Name = "SuperAdmin",
                Password = "SuperAdmin"
            };

            // act
            IActionResult result = await controler.Login(loginInfo);

            // assertions
            Assert.IsNotNull(result);
            var okObjectResult = result as OkObjectResult;
            var credential = okObjectResult.Value.GetType().GetProperty("credential").GetValue(okObjectResult.Value) as Teacher;
            var token = okObjectResult.Value.GetType().GetProperty("token").GetValue(okObjectResult.Value) as string;
            Assert.IsNotNull(credential);
            Assert.IsNotNull(token);
            credential.Name.Should().Be("SuperAdmin");
            credential.Password.Should().Be("SuperAdmin");
            credential.Role.Should().Be("SuperAdmin");

        }

    }
}