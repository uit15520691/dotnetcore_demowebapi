﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Contexts;
using WebApi.Models;

namespace WebApi.Service
{
    public class TeacherService
    {
        private readonly ApplicationContext _context;

        public TeacherService(ApplicationContext context)
        {
            _context = context;
        }


        public async Task<Teacher> GetLoginTeacherAsync(Teacher loginCredentials)
        {
            var teacher = await _context.Teachers
                        .Where(t => t.Name == loginCredentials.Name && t.Password == loginCredentials.Password)
                        .SingleOrDefaultAsync();
            return teacher;
        }
        

    }
}
