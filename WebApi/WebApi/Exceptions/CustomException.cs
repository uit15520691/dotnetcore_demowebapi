﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using WebApi.Enum;

namespace WebApi.Exceptions
{
    public class CustomException : Exception, ISerializable
    {
      
        public CustomExceptionEnum ErrorCode { get; set; }
        public CustomException(String message, CustomExceptionEnum errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }

        public CustomException() { }
        public CustomException(CustomExceptionEnum errorCode) 
        {
            ErrorCode = errorCode;
        }
    }
}
