﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WebApi.Enum;

namespace WebApi.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DayOfWeekEnum Date { get; set; }
        public Room Room { get; set; }
        public Teacher Teacher { get; set; }
        public List<CourseStudent> CourseStudents { get; set; }
    }
}
