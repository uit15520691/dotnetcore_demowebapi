﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Room
    {
        [Key]
        public string Number { get; set; }
    }
}
