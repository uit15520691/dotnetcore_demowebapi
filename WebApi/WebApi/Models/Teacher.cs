﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Teacher
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
