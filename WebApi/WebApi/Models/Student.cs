﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Student
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public bool GoodStudent { get; set; }
        public List<CourseStudent> CourseStudent { get; set; }
    }
}
