﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class CourseStudent
    {
        [Key]
        public int Id { get; set;}
        public Course Course { get; set; }
        public Student Student { get; set; }
    }
}
