﻿using Microsoft.AspNetCore.Authorization;

namespace WebApi.Security
{
    public class Policies
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string SuperAdminPassword = "SuperAdmin";
        public const string Admin = "Admin";
        public const string User = "User";

        public static AuthorizationPolicy SuperAdminPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(SuperAdmin).Build();
        }
        public static AuthorizationPolicy AdminPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Admin).Build();
        }

        public static AuthorizationPolicy UserPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(User).Build();
        }
    }
}
