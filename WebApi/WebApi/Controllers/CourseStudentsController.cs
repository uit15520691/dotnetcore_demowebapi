﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Contexts;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseStudentsController : ControllerBase
    {
        private readonly ApplicationContext _context;

        public CourseStudentsController(ApplicationContext context)
        {
            _context = context;
        }

        // GET: api/CourseStudents
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CourseStudent>>> GetCourseStudents()
        {
            return await _context.CourseStudents.ToListAsync();
        }

        // GET: api/CourseStudents/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CourseStudent>> GetCourseStudent(int id)
        {
            var courseStudent = await _context.CourseStudents.FindAsync(id);

            if (courseStudent == null)
            {
                return NotFound();
            }

            return courseStudent;
        }

        // PUT: api/CourseStudents/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCourseStudent(int id, CourseStudent courseStudent)
        {
            if (id != courseStudent.Id)
            {
                return BadRequest();
            }

            _context.Entry(courseStudent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseStudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CourseStudents
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CourseStudent>> PostCourseStudent(CourseStudent courseStudent)
        {
            _context.CourseStudents.Add(courseStudent);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCourseStudent", new { id = courseStudent.Id }, courseStudent);
        }

        // DELETE: api/CourseStudents/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CourseStudent>> DeleteCourseStudent(int id)
        {
            var courseStudent = await _context.CourseStudents.FindAsync(id);
            if (courseStudent == null)
            {
                return NotFound();
            }

            _context.CourseStudents.Remove(courseStudent);
            await _context.SaveChangesAsync();

            return courseStudent;
        }

        private bool CourseStudentExists(int id)
        {
            return _context.CourseStudents.Any(e => e.Id == id);
        }
    }
}
