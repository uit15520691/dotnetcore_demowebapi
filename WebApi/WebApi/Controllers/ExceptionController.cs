﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NLog;
using WebApi.Contexts;
using WebApi.DTO;
using WebApi.Exceptions;
using WebApi.Models;
using WebApi.Security;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExceptionController : ControllerBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ExceptionController()
        {

        }
        [HttpGet]
        [Route("/getOK")]
        public ActionResult GetExceptionOK()
        {
            logger.Info("Ready to throw custom exception with status code 200");
            throw new CustomException("custom message", Enum.CustomExceptionEnum.MY_OK);
        }

        [HttpGet]
        [Route("/getForbidden")]
        public ActionResult GetExceptionForbidden()
        {
            logger.Warn("Ready to throw custom exception with status code 403");
            throw new CustomException("custom message", Enum.CustomExceptionEnum.MY_FORBIDDEN);
        }

        [HttpGet]
        [Route("/getNotFound")]
        public ActionResult GetExceptionNotFound()
        {
            logger.Error("Ready to throw custom exception with status code 404");
            throw new CustomException("custom message", Enum.CustomExceptionEnum.MY_NOT_FOUND);
        }
    }
}
