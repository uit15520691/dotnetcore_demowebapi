﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApi.Models;
using WebApi.Security;
using WebApi.Service;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly TeacherService _teacherService;

        public LoginController(IConfiguration config, TeacherService teacherService)
        {
            _config = config;
            _teacherService = teacherService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]Teacher login)
        {
            IActionResult response = Unauthorized();
            Teacher teacher;
            if (Policies.SuperAdmin.Equals(login.Name) && Policies.SuperAdminPassword.Equals(login.Password))
            {
                teacher = login;
                teacher.Role = Policies.SuperAdmin;
            } else
            {
                teacher = await _teacherService.GetLoginTeacherAsync(login);
            }
            if (teacher != null)
            {
                var tokenString = GenerateJWToken(teacher);
                response = Ok(
                    new
                    {
                        token = tokenString,
                        credential = teacher
                    }
                );
            }
            return response;
        }

        private async Task<Teacher> AuthenticateTeacher(Teacher loginCredentials)
        {
            Teacher teacher = await _teacherService.GetLoginTeacherAsync(loginCredentials);
            return teacher;
        }

        private string GenerateJWToken(Teacher teacherInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:SecretKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, teacherInfo.Name),
                new Claim("role", teacherInfo.Role),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        
    }
}
