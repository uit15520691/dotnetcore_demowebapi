﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text;
using WebApi.Enum;
using WebApi.Exceptions;

namespace WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ExceptionHandlerController : ControllerBase
    {
        [Route("error")]
        public void Error()
        {
            HttpResponse response = HttpContext.Response;
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;
            var code = 500;

            if (exception is CustomException)
            {
                var customException = exception as CustomException;
                switch (customException.ErrorCode)
                {
                    case CustomExceptionEnum.MY_OK:
                        code = 200;
                        break;
                    case CustomExceptionEnum.MY_NOT_FOUND:
                        code = 404;
                        break;
                    case CustomExceptionEnum.MY_FORBIDDEN:
                        code = 403;
                        break;
                }
            }
            response.StatusCode = code;
            response.WriteAsync(new {exception.Message}.ToString(), Encoding.UTF8);
        }
    }
}
