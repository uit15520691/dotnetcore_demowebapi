﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.DTO
{
    public class StudentDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
