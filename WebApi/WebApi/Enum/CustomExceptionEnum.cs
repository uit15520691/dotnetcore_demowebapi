﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Enum
{
    public enum CustomExceptionEnum
    {
        MY_NOT_FOUND,
        MY_FORBIDDEN,
        MY_OK
    }
}
