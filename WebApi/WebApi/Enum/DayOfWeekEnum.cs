﻿namespace WebApi.Enum
{
    public enum DayOfWeekEnum
    {
        Monday, 
        Tuesday, 
        Wednesday, 
        Thursday,
        Friday, 
        Saturday,
        Sunday
    }
}
