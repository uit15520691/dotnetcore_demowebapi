import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { JWTService } from '../../commons/jwt/jwt.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  loginForm: FormGroup;

  constructor(
    private accountService: AccountService,
    private formBuilder: FormBuilder,
    private jwtService: JWTService
    ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
    console.log(this.jwtService.jwtToken);
  }

  get f() {
    return this.loginForm.controls;
  }

  async login(): Promise<void> {
    var loginInfo = {
      name: this.f.userName.value,
      password: this.f.password.value
    };
    this.accountService.login(loginInfo).subscribe(
      data => {
        this.jwtService.setToken(data.token);
      },
      error => {
        console.log(error);
      }  
    );
  }

  async test(): Promise<void> {
    this.accountService.test().subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }


}
