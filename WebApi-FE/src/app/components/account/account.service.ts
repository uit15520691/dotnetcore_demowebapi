import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http"
import { AccountLogin } from "./types/AccountLogin";


@Injectable({
    providedIn: 'root'
})
export class AccountService {
    constructor(private http: HttpClient) {
    }
    public login(accountLogin: AccountLogin) {
        return this.http.post<any>("/api/login", accountLogin);
    }

    public test() {
        return this.http.get<any>("/api/student");
    }
}