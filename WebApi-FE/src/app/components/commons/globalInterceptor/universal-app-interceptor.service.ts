import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http'
import { JWTService } from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class UniversalAppInterceptorService implements HttpInterceptor {

  constructor(private jwtService: JWTService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    req = req.clone({
      url:  req.url,
      setHeaders: {
        Authorization: `Bearer ${this.jwtService.jwtToken}`
      }
    });
    return next.handle(req);
  }
}
