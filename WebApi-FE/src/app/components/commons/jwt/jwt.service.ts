import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode'

@Injectable({
  providedIn: 'root'
})
export class JWTService {

  constructor() { }

  jwtToken: string;
  decodedToken: { [key: string]: string};

  setToken(token: string) {
    if (token) {
      this.jwtToken = token;
    }
  }

  decodeToken(token: string = this.jwtToken) {
    if (token) {
      this.decodedToken = jwt_decode(token);
    }
  }

  getDecodeToken() {
    return jwt_decode(this.jwtToken);
  }

}
